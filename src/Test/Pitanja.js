const Pitanja = [
	{
		id: 0,
		pitanje: "Kakva kafa treba da bude?",
		odgovori: ["Kisela","Gorka"],
		tacanOdg: "Kisela"

	},
	{
		id: 1,
		pitanje: "Kafa je:",
		odgovori: ["Voće", "Povrće"],
		tacanOdg: "Voće"

	},
	{
		id: 2,
		pitanje: "Espresso je:",
		odgovori: ["Vrsta kafe", "Način pripreme"],
		tacanOdg: "Način pripreme"

	},
	{
		id: 3,
		pitanje: "Stablika kafe je visoka između:",
		odgovori: ["2-5 metara", "3-8 metara", "1-3 metara", "6-9 metara"],
		tacanOdg: "2-5 metara"

	},
	{
		id: 4,
		pitanje: "Cvet kafe je:",
		odgovori: ["cnr", "Beo", "crven"],
		tacanOdg: "beo"

	},
	{
		id: 5,
		pitanje: "Na kojoj nadmorskoj visini Arabika uspeva?",
		odgovori: ["500-1000 nmv", "600-2000 nmv", "800-1600 nmv", "2000-3500 nmv"],
		tacanOdg: "600-2000 nmv"

	},
	{
		id: 6,
		pitanje: "Koja kolicina kafe se dobija sa jednog drveta?",
		odgovori: ["1-3 kg", "2-4 kg", "10-15 kg", "15-20 kg"],
		tacanOdg: "1-3 kg"

	},
	{
		id: 7,
		pitanje: "Na kojoj temperaturi se priprema Espresso?",
		odgovori: ["60-70 °C", "75-80 °C", "85-90 °C", "90-95 °C"],
		tacanOdg: "60-70 °C"

	},
	{
		id: 8,
		pitanje: "Koji je minimalni pritisak pumpe?",
		odgovori: ["7 bara","9 bara", "5 bara", "12 bara"],
		tacanOdg: "9 bara"

	},
	{
		id: 9,
		pitanje: "Koja kolicina kafe se koristi pri pravljenju espresso kafe?",
		odgovori: ["5g","7g","9g","11g"],
		tacanOdg: "7g"

	},
	{
		id: 10,
		pitanje: "Temperatura kafe koja se sipa u soljicu treba da bude:",
		odgovori: ["75-80 °C","50-60 °C","80-95 °C","60-65 °C"],
		tacanOdg: "75-80 °C"

	},
	{
		id: 11,
		pitanje: "Šta utiče na kremu?",
		odgovori: ["Temperatura šoljice","Zrno kafe","Mleko","Voda"],
		tacanOdg: "Temperatura šoljice"

	},
	{
		id: 12,
		pitanje: "Jačina sabijanja bi trebalo da bude oko:",
		odgovori: ["5kg","10kg","15kg","20kg"],
		tacanOdg: "20kg"

	},
	{
		id: 13,
		pitanje: "Macchiato se služi u:",
		odgovori: ["Maloj šoljici","Velikoj šoljici","Čaši"],
		tacanOdg: "Maloj šoljici"

	},
	{
		id: 14,
		pitanje: "Mleko koje se dodaje u Caffe Latte macchiato:",
		odgovori: ["1:1 - mleko : kafa","2:1 - mleko : kafa","3:1 - mleko : kafa"," 4:1 - mleko : kafa"],
		tacanOdg: "4:1 - mleko : kafa"

	},
	{
		id: 15,
		pitanje: "Eiskaffee je:",
		odgovori: ["Hladna kafa","Hladna kafa sa sladoledom","Vruca kafa","Vruca kafa sa viskijem"],
		tacanOdg: "Hladna kafa sa sladoledom"

	},
	{
		id: 16,
		pitanje: "Konsul je kafa sa:",
		odgovori: ["Sladoledom","Šlagom","Slatkom pavlakom","Likerom"],
		tacanOdg: "Slatkom pavlakom"

	},
	{
		id: 17,
		pitanje: "Koliko vrsta kafa postoji?",
		odgovori: ["7 vrsta","12 vrsta","48 vrsta","vise od 50 vrsta"],
		tacanOdg: ""

	}

];

export default (n = 4) =>
    Promise.resolve(Pitanja.sort(() => 0.5 - Math.random()).slice(0,n));