import React, { Component } from "react";
 
class Saveti extends Component {
  render() {
    return (
      <div className="row" id="content">
        <div className="col l6">
          <h1>Kafa!?</h1>
            <p>Kad vam kažu: „Kafa!“, na šta prvo pomislite? Na kratki espresso, domaću ili tursku, ili na pola litre toplog napitka zaslađenog sladoledom ili šlagom? Po jednoj interesantnoj studiji Judi James i James Moore, vrsta kafe koju pijete mnogo govori o vama, o nivou samopouzdanja, odnosu prema stresnim situacijama, pa čak i o seksualnom životu.</p>
            <img id="imageSaveti" src="../img/img5.jpg" alt="Saveti"/>
          <br />
        
          <h1>Saveti</h1>
          <h2>Kako napraviti savršenu šoljicu espresso kafe</h2>
          <p>Espresso kafa mora da ima sledeće karakteristike:</p>
          <p>Pena ili crema, kako je zovu Italijani, simbol je po kome se espresso odvaja od ostalih metoda pripreme. Pena na vrhu espressa nastaje usled mešanja ugljen dioksida (CO2) sa uljima kojih ima dosta u kafi. Cremu zapravu i čini oko 200.000 mehurića ugljen dioksida. Zabluda je da dobar espresso ima veliku postojanu penu. Naprotiv – to je odlika Robuste. Crema je debljine oko tri milimetra, mora biti kompaktna i prilikom mešanja kašičicom ona se ne razbije. Veoma je bitno da crema ima jedinstvenu boju lešnika. Crema predstavlja 10 odsto količine cele kafe u šoljici.</p>
          <p>Body (gustina, jačina, masa) – prvenstveno zavisi od ulja koje kafa sadrži. Neophodno je da espresso ima konstantnu gustinu, doprinoseći punoći napitka i ublažavajući gorak ukus</p>
          <p>Aroma – espresso kafa ima izraženu, prefinjenu aromu. Taj prijatan ukus ostaje još neko vreme nakon ispijanja espresso kafe. Espresso aroma je očekivana zbog prisustva pene koja hvata isparljivu supstancu i čuva je od brzog nestajanja posle sipanja espresso kafe u šolju.</p>
          <img id="imageSaveti" src="../img/img4.jpg" alt="Saveti"/>
          <br />
          <br />
        
        <h2>Savršena šoljica espressa</h2>
        <br />
        <p>Kod espresso kafe se ceni kiselost, a taj prijatan ukus kiselosti ima Arabica. Savršeni espresso obično je prekriven penom od dva do četiri milimetara, koja je boje lešnik braon i zadržava se oko dva minuta. Ključne stvari za pripremu savršenog espressa su:</p>
        <p>Temperatura 90-95 °C</p>
        <p>Pritisak pumpe 9 bara</p>
        <p>7 –7,5 grama kafe</p>
        <br />
        <p>Pored ova tri ključna faktora za savršen espresso, potrebno je ispuniti dodatne uslove:</p>
        <p>– Temperatura kafe koja se sipa u šoljicu bi trebalo da bude između 75 i 80°C</p>
        <p>– Volumen u šoljici je između 25 i 30 cc</p>
        <p>– Vreme ekstrakcije traje od 25 do 30 sekundi. Sve preko 30 sekundi dovodi do lošeg ukusa kafe. Na početku ekstrakcije, kafa je izuzetno kisela. Što je duža ekstrakcija, kafa je gorča</p>
        <p>– Pritisak sabijanja može da utiče na vreme ekstrakcije. Vreme ekstrakcije može da se skrati slabijim sabijanjem ili produži jačim. Pritisak sabijanja može da promeni vreme ekstrakcije i do šest sekundi. Jačina sabijanja bi trebalo da bude oko 20 kilograma</p>
        <p>– Šoljice moraju biti od belog porcelana</p>
        <p>– Veoma je bitno da temperatura šoljice bude 35-40°C. Toplota šoljice utiče na cremu. Hlađenjem espresso gubi cremu, boju i aromu, a pojačava se kiselost</p>
        <p>– Pritisak u bojleru aparata mora biti od 0,9 do 1,3 bara</p>
        <br />
        
        </div>
      </div>
    );
  }
}
 
export default Saveti;