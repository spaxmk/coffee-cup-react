import React, { Component } from "react";
 
class Varijacije extends Component {
  render() {
    return (
      <div class="row" id="content">
        <div class="col-12">
          <h1>Varijacije</h1>
          <br />
        </div>
        
        <div class="col-4">
          <img id="imageVrste" src="../img/espresso/img1.jpg" alt="espresso"/>
        </div>
        <div class="col-8">
          <h6>Macchiato</h6>
          <p>Kada na osnovu kafe, odnosno na espreso dodamo kašiku pene od vručeg mleka dobijemo jedno od najpopularnijih napitaka od kafe na našim prostorima. Macchiato. Potrebna doza kafe je 7g, a proces spravljanja kafe je isti kao kod espreso. Ovaj napitak se služi u male šoljice za espreso kafu.</p>
        </div>

        <div class="col-8">
          <br />
          <h6>Caffe Latte</h6>
          <p>Količina kafe koje je potrebna za spravljanje je 7g odnosno jedna doza kafe. Mleko koje se dodaje je vruče mleko koje grejete na pari. Kada se stvori gusta krema od mleka, mleko pažljivo sipamo preko espreso kafu u odnosu 3:1 - mleko : kafa. Služi se u široke velike šoljice. Mnogi ovaj napitak zovu i cappuchino bez čokolade :)</p>
        </div>
        <div class="col-4">
          <img id="imageVrste" src="../img/espresso/img2.jpg" alt="espresso"/>
        </div>

        <div class="col-4">
          <img id="imageVrste" src="../img/espresso/img3.jpg" alt="espresso"/>
        </div>
        <div class="col-8">
          <h6>Cafe Latte macchiato</h6>
          <p>Ova varijacija Caffe Latte služi se isključivo u visoke čaše radi vizualnog dojma. Prakticira se prvo sipanje mleka, zatim lagano sipanje kafe preko. Tada dobijemo vizualni dojam da nasuta kafa „lebdi“. Prije konzumiranja potrebno promiješati napitak, a omjer mleka i kafe je 4:1</p>
        </div>

        <div class="col-8">
          <br />
          <h6>Cappuchino</h6>
          <p>Postupak je isti kao i za spravljanje Caffe Latte. Praktikuje se za lakše dobivanje pene odnosno kreme od mleka korištenje punomasnog mleka koje se zagrijava na pari aparata. Količina kafe koje je potrebna za spravljanje je 7g odnosno jedna doza kafe, a omjer mleka i kafe 3:1. Serviranje je u široku veliku šalicu, a preko pene se lagano posipa narendana čokolada ili cimet.</p>
        </div>
        <div class="col-4">
          <img id="imageVrste" src="../img/espresso/img4.jpg" alt="espresso"/>
        </div>

        <div class="col-4">
          <img id="imageVrste" src="../img/espresso/img5.jpg" alt="espresso"/>
        </div>
        <div class="col-8">
          <h6>Caffe Mocha</h6>
          <p>Jedna kašika čokolade u prahu na dnu velike šoljice prvi je korak u spravljanju ovog napitka od kafe. Naredni korak je po želji izbor celog procesa i vrste kafe koju želimo piti bilo da se radi o Caffe Latte, Caffe Latte Macchiato ili Cappuchino.</p>
          <br />
        </div>

        <div class="col">
          <br />
          <h4>Razne vrste kafa</h4>
          <br />
          <p> <b>Café granite</b> je skuvana jaka, zasladjena kafa, zatim zamrznuta, pa izmrvljena i u soljici prelivena likerom od kafe.</p> 
          <p> <b>Cefe orange</b> je kafa dopunjena slagom i likerom od pomorandze. </p>
          <p> <b>Café royal</b> je jaka, zasladjena kafa, flambirana konjakom i rakijom. </p>
          <p> <b>Caffe macchiato</b> je kafa prelivena s malo mleka. </p>
          <p> <b>Café brulot</b> je jaka kafa sipana u soljicu ipreko izmrvljenog leda sa kasicicom maraskina, ili konjaka, ili ruma, a moze biti i flambirana. </p>
          <p> <b>Café americano</b> je vodom razredjena espreso kafa. </p>
          <p> <b>Caffe corretto</b> je kafa sa dodatkom lozovace (u severnoj Italiji) ili sa likerom od badema (na jugu Italije). </p>
          <p><b> Cappuccino</b> je espreso kafa sa vrelim mlekom koje je pod parom zapenilo, posuto kakao prahom. </p>
          <p> <b>Eiskaffee</b> je hladna kafa sa sladoledom od vanile, slagom i izmrvljenim ledom. </p>
          <p> <b>Irish coffee</b> je irski viski posluzen uz secer i kafu sa slagom, slamkom i kasicicom. </p>
          <p> <b>Kaffee verkehrt</b> ili obrnuta kafa je pripremljena od vise mleka nego kafe, kao svetla bela kafa. </p>
          <p> <b>Kaisermelange</b> ili carska mesavina je zumance umuceno sa secerom, cemu je dodato malo mleka i konjaka, preliveno crnom kafom. </p>
          <p> <b>Konsul</b> je kafa sa malo tecne slatke pavlake. </p>
          <p> <b>Melange</b> je kafa izmesana sa istom kolicinom mleka, zasladjena secerom ili medom, sluzi se u velikim soljama. </p>
          <p> <b>Noisette</b> je crna kafa ili espreso kafa sa samo nekoliko kapi mleka. </p>
          <p> <b>Pharisaer</b> je zagrejan rum, preliven jakom kafom sa secerom i slagom. </p>
        

        </div>
      </div>  
    );
  }
}
 
export default Varijacije;