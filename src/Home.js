import React, { Component } from "react";
 
class Home extends Component {
  render() {
    return (
      <div class="row" id="content">
        
        <div class="col-4">
          <h2 className="center">Srce u šolji kafe</h2>
          <p>Ako mene pitate – kafa je čarobni napitak. O njoj bih mogla pisati priče i pjesme, pila bih je u svakom trenutku dana i noći, a kada bih morala birati jedno piće za do kraja života – velika dilema bi bila između nje i vode. Da, znam, pretjerujem. Ali ko još zna biti umjeren u konzumirajućim ljubavima?</p>
          <p></p>
          <p>S obzirom na tu opčinjenost i hedonističku nastrojenost, bilo je pitanje vremena kada će se nešto kafeno pronaći ovdje u ovom mom internet kutku, a trenutak je došao sasvim spontano kada sam u jednom od najdražih banjalučkih lokala (ako vas baš zanima kojem – u Pauzi) sjedila i pričala sa kolegom koji ima zvanje bariste. Ideja je došla sama od sebe: hajde da napravimo priču o kafi, hajde da napravimo fotke od kojih će svako poželjeti istog trenutka da prisloni šoljicu kafe na usne, da napravimo video koji će pokazati ljepotu i pedantnost pravljenja kafe i latte arta i da napišemo tekst koji će nas naučiti svakakvim činjenicama o kafi, ali ujedno i zabaviti.</p>
        </div>
        <div class="col-8">
          <img id="imageRest" src="../img/img1.jpg" alt="Srce u soljici"/>
        </div>
      

        
        
          <div className="col-7">
            <img id="imageRest" src="../img/img2.jpg" alt="Srce u soljici"/>
          </div>
          <div className="col-5">
            <h2 className="center">Šoljica kafe je šoljica ljubavi</h2>
            <p>Prvo i osnovno pitanje kojim treba da se bavimo jesu osnovne informacije o kafi koje svi znamo ili ne znamo – a treba da znamo. Kafa je biljka koja spada u red niskih iz porodice broćeva (Rubiaceae), a najviše joj pogoduje klima u tropskim zemljama koje se nalaze u širem ekvatorijalnom pojasu. Njena stabljika je visoka od 2 do 5 metara, a u divljini može narasti čak i do 10. Cvjetovi kafe su mali i bijeli, rastu u skupinama duž grane i, naravno, mirisni su. Unutar ploda, kojeg karakteriše čvrst omotač, nalaze se dva sjemena koja svojom pozicioniranošću podsjećaju na plod oraha. Sama boja ploda može varirati od zelene do smeđe, a najpoznatiji sastojak zrna kafe je kofein.</p>
            <p>Sirova kafa ima veoma oštar ukus, tako da se prava svojstva, ukus i miris dobijaju tek kada prođe postupak obrade koji uključuju termičku obradu, prženje i hlađenje. Termička obrada predstavlja sušenje zrna kafe, a prženje fazu tokom koje kafa mijenja boju, smanjuje vlažnost, gubi na težini i dobija određenu aromu. U suštini tada se dešavaju fizičke i hemijske promjene, a u tome najbitniju ulogu igra temperatura – ona određuje kakva će biti boja zrna i druga njegova obilježja. U zavisnosti od načina prženja dobijaju se i različite kafe. Na samom kraju dolazi hlađenje ispržene kafe vazduhom ili hladnom vodom.</p>
            <br />
            <br />
          </div>

          <div className="col-4">
            <h2 className="center">Ispijanje kafe kao hedonistička procedura</h2>
              <p>Neki ispiju kafu u jednom gutljaju – jer tako treba, neki piju po dva sata, neki vole čokoladu uz nju, neki stave mnogo šećera, a neki kažu da je to skrnavljenje. Zato pitah Marka da li postoji neka određena procedura i pravila kao ona kod vina.</p>
              <p></p>
              <p>Procedura ne postoji, samo se u nekim krajevima svijeta stvorila tradicija ispijanja kafe. Ti tradicionalni načini se ogledaju u vrstama napitaka i određenim prilikama za ispijanje kafe. Negdje se više pije filter kafa, samim tim proces ispijanja traje nešto duže nego tamo gdje se preferira espresso ili cappuccino. Bitno je da je proizvod kvalitetan, a kako će ko popiti svoju kafu ide od čovjeka do čovjeka.</p>
          </div>
          <div className="col-8">
              <img id="imageRest" src="../img/img7.jpg" alt="Srce u soljici"/>
              <br />
          <br />
          </div>
          

            <div className="col-8">
              <img id="imageRest" src="../img/img6.jpg" alt="Srce u soljici"/>
            </div>
            <div className="col-4">
              <h2 className="center">Zanimljivosti o kafi koje ste možda znali – ili ipak niste?</h2>
              <ul>
                <li>Kafa je biljka, kafa je voće.</li>
                <li>Kafa treba da je kisela, a ne gorka.</li>
                <li>„Kratka“ kafa ima manje kofeina od „produžene“ kafe.</li>
                <li>Kafa je psihoaktivna supstanca.</li>
                <li>Espresso nije posebna vrsta kafe, nego način na koji se kafa priprema.</li>
                <li>U Africi se zrna kafe natope u vodu, pomiješaju sa začinima i zatim služe kao bombone.</li>
                <li>Na svijetu postoji više od 50 vrsta kafe.</li>
                <li>40% kafe u svijetu proizvodi se u Brazilu i Kolumbiji.</li>
                <li>1645. godine je u Italiji otvorena prva kafana.</li>
                <li>Kopi Luwaki je jedna od najrjeđih sorti kafe koja se godišnje proizvode u vrlo malim količinama, a 100g iste prodaje se za 75 USD. Pravi se na ostrvima Sumatra, Java i Sulavesi tako što cibetke (torbari) jedu plod kafe koji unutar digestivnog trakta i usljed enzima iz želuca dobije specifičan ukus. Polusvarena zrna se skupljaju i od njih se pravi ova rijetka i veoma skupa kafa.</li>
              </ul>
            </div>

            <div className="col-4">
              <h2 className="center">Kako prepoznati dobru kafu?</h2>
              <p>Ukoliko kafu pijemo bez dodataka, trebalo bi da ima postojanu kremu na vrhu te da bude služena na temperaturi od 87 do 93 stepena Celzijusa, kaže Marko. Šoljica bi trebalo da bude topla, nipošto hladna. Ukoliko kafu pijemo sa mlijekom, teže se primjećuju nedostaci, ali poželjno je ne pregrijavati mlijeko (60 do 70 stepeni Celzijusa) i obavezno koristiti svježe.</p>
              <p></p>
              <p>Svakako, najbolji način da utvrdite da je poslužena kafa dobra jeste taj da otpijete gutljaj. Ako se nakon istog pojavi osmijeh na licu, a vaše nepce bude uživalo i nakon nekoliko sekundi – tražeći još, a vi ste onda zasigurno dobili onu pravu.</p>
            </div>
            <div className="col-8">
              <img id="imageRest" src="../img/img3.jpg" alt="Srce u soljici"/>
              <br />
            </div>
            

      </div>

      
    );
  }
}
 
export default Home;