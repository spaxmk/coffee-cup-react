import React from "react";

const Rezultat = ({rezultat, TestPonovo}) => (
	<div className="col s12">
		
		<br />
		<h2 className="center">Odgovorili ste tačno na {rezultat} od 4 postavljena pitanja!</h2>
		<br />
		
		<button className="btn btn-dark col s12" onClick={TestPonovo}>
			Nova provera znanja!
		</button>
	</div>
);

export default Rezultat;