import React, { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  Route,
  NavLink,
  HashRouter
} from "react-router-dom";
import Home from "./Home";
import Saveti from "./Saveti";
import Varijacije from "./Varijacije";
import Vrste from "./Vrste";
import Footer from "./Footer";
import Test from "./Test";

class Main extends Component {
  render() {
    return (
      <HashRouter>
        <div>
          <img id="imageHeader" src="../img/cover.jpg" alt="Srce u soljici" width="100%" height="auto"/>
          <ul className="header">
            <li><NavLink exact to="/">Pocetna</NavLink></li>
            <li><NavLink to="/saveti">Saveti</NavLink></li>
            <li><NavLink to="/vrste">Vodic za vrste</NavLink></li>
            <li><NavLink to="/varijacije">Varijacije</NavLink></li>
            <li><NavLink to="/test">Provera znanja</NavLink></li>
          </ul>
          <div className="content">
            <Route exact path="/" component={Home}/>
            <Route path="/saveti" component={Saveti}/>
            <Route path="/vrste" component={Vrste}/>
            <Route path="/varijacije" component={Varijacije}/>
            <Route path="/test" component={Test}/>
          </div>
        </div>
        <Footer />
      </HashRouter>
    );
  }
}
 
export default Main;