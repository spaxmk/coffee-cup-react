import React, { useState } from "react";

const Pitanje = ({ pitanje, odgovori, odgovoreno}) =>{
	const [odgovor, setOdgovor] = useState(odgovori);
	return (
	    <div className="col s12">
	    	<div className="col s3">         
		        <h4>{pitanje}</h4>
		        {odgovor.map((text, index) => (
		        	<button 
		        		id="Testdugme"
		        		key={index} 
		        		className="btn btn-dark col s4" 
		        		onClick={() => {
		        			setOdgovor([text]);
		        			odgovoreno(text);
		        		}}>
		        		{text}
		        	</button>
		        ))}		
		    </div>
		        <br />	          	
		</div>
	);
}
 
export default Pitanje;