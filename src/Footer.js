import React, { Component } from "react";
 
class Footer extends Component {
  render() {
    return (
      <footer className="page-footer">
          <div className="container-fluid text-center text-md-left">
            <div className="row">
              <div className="col-6">
                <h5 className="white-text">Razvoj web aplikacija</h5>
                <br />
                <p className="grey-text text-lighten-4">Ispitni projekat iz predmeta Razvoj web aplikacija.</p>
                <p className="grey-text text-lighten-4">Master studije - Informacione Tehnologije</p>
              </div>
              <div className="col-3">
                <h5 className="white-text">Literatura</h5>
                <ul>
                  <li><a className="grey-text text-lighten-3" target="_blank" rel="noopener noreferrer" href="http://kafomanija.com/espresso/napici-od-espressa/">Kafomanija</a></li>
                  <li><a className="grey-text text-lighten-3" target="_blank" rel="noopener noreferrer" href="https://katarinamiladinovic1.wordpress.com/">Katarina Miladinovic</a></li>
                  <li><a className="grey-text text-lighten-3" target="_blank" rel="noopener noreferrer" href="https://lovily.net/srce-u-solji-kafe-ili-sve-o-kafi/">Lovily.net</a></li>
                  <li><a className="grey-text text-lighten-3" target="_blank" rel="noopener noreferrer" href="https://magazin.ba/zanimljivosti/moze-jedna-kafa-vrsta-kafe-koju-pijete-mnogo-govori-o-vama-20078.html">Magazin.ba</a></li>
                </ul>
              </div>
              <div className="col-3">
                <ul>
                  <br />
                  <li><a className="grey-text text-lighten-3" target="_blank" rel="noopener noreferrer" href="https://kafaiaparati.weebly.com/vrste-kafe-i-priprema.html">Kafeaparati</a></li>
                  <li><a className="grey-text text-lighten-3" target="_blank" rel="noopener noreferrer" href="https://www.w3schools.com/REACT/default.asp">W3 Schools | REACT</a></li>
                  <li><a className="grey-text text-lighten-3" target="_blank" rel="noopener noreferrer" href="https://www.google.com/search?q=hungarian+cards+deck">Google - Pretraga slika</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div className="footer-copyright">
            <div className="container">
            © 2020 Spasoja Stojšić
            </div>

          </div>
        </footer>
    );
  }
}
 
export default Footer;