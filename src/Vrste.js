import React, { Component } from "react";
 
class Vrste extends Component {
  render() {
    return (
        <div class="row" id="content">
          <h1>Vrste kafe i priprema</h1>
          <br />
          <p>Kafa (arapski: kahva) je napitak koji se priprema kuvanjem prženih semenki biljke kafe, najčešće u vodi ili u mleku. Kafa se obično služi topla. Ovo piće je veoma popularno u mnogim zemljama sveta. Postoji više načina pripremanja kafe, a među najpoznatijima su turska kafa – kod nas sve češće „domaća“, filter kafa, espresso, instant kafa i kapućino.</p>
          <p>Ako volite da okrenete šolju domaće kafe pošto je ispijete na iskap, kako bi vam nečije vešto i maštovito oko reklo šta vas čeka u budućnosti, onda će vas sigurno zanimati i kakav vam je karakter u zavisnosti od toga kakvu kafu pijete.</p>
          <p>Crna kafa koju većina nas svakodnevno pije mešavina je više vrsta ove biljke. Iza jednostavnog naziva KAFA zapravo se krije mešavina dve biljke iste botaničke grupe ali različite vrste, poznate kao Arabika i Robusta.</p>
          <br />
          <hr />
          <br />
          <div class="col-8">
            <h5>ARABIKA (coffea Arabica linaeus)</h5>
            <br />
            <p>Arabika je veoma kvalitetna kafa, izvrsnih svojstava. Na nju otpada 75-80% ukupne svetske proizvodnje. Proizvodi se na visinskim terenima od 600 do 2000 m nadmorske visine. U proirodi rađa na drvetu koje može da poraste i do 6 metara, ali se stablo kafe na plantažama, radi lakšeg branja, skraćuje na visinu do 3 metra. Cveta posle 3 godine od sadnje, a naredne godine daje prvu berbu. Punu rodnost dostiže tek u šestoj godini. Sa jednog drveta se dobija od 1-3 kg kafe, a glavna berba u Brazilu je od maja do avgusta. 
              Kod nas, kao i u svetu, najpoznatije vrste arabike su "minas" i " santos", što su imena izvedena iz oblasti Brazila u kojima se one uzgajaju. Pored ovih postoji i veliki broj drugih vrsta – "sigri" sa Nove Gvineje, potomak "plave planine" sa Jamajke; "maragogip" iz Meksika sa najvećim zrnima i najmanjim sadržajem kofeina; blaga "kostarika"; "moka harar" iz Etiopije sa ukusom koji podseća na čokoladu; "AA džambo" iz Kenije je najbolja afrička arabika.</p>
          </div>
          <div class="col-4">
            <img id="imageVrste" src="../img/img8.jpg" alt="Arabica"/>
          </div>
        <br />
        <br />

          <div class="col-4">
            <img id="imageVrste" src="../img/img9.jpg" alt="Arabica"/>
          </div>
          <div class="col-8">
            <h5>ROBUSTA ( coffea Canehora Pirex Francher )</h5>
            <br />
            <p>Robusta se uzgaja u tropskim predelima Afrike, Indije, Indonezije i Vijetnama i to na nižim terenima (do 600 m.n.v.). Ona je dominantna na Afričkom kontinentu jer se lakše uzgaja, brže sazreva i otporna je na bolesti i štetočine. Na Robustu u svakoj proizvodnji otpada 20-25% i ona je znatno jeftinija. Razlikuje se od Arabike po krupnoći i izgledu zrna koja su svetlo braon boje, okruglasta i nepravilnog oblika. Sitnija je od Arabike i lošijeg je izgleda jer se priprema po suvom postupku bez pranja i mehaničkim odvajanjem od ljuske. Sadrži više otpadnih primesa i crnih zrna. Napitak od čiste Robuste nije zadovoljavajućegi ukusa i ne može čista da se koristi za spravljanje napitka kafe . Opor je i gorak i nema izraženu aromu, i koristi se u mešavinama jer svojim ekstraktom i visokim procentom kofeina (2,3-3,5%) napitku daje bogat ukus. Robusta se koristi za spravljanje mešavina kafe sa Arabikom, ali je njeno učešće manje. Ona napitku daje punoću, dok aroma crne kafe potiče od Arabike. Velike količine Robuste koriste se u proizvodnji ekstrakta kafe. Najveći proizvođači i izvoznici Robuste su: Vijetnam, Indonezija, Uganda, Indija i Obala Slonovače.</p>
          </div>
          <br />
          <h5>Prženje kafe</h5>
          <p>Proces prženja kafe je najzahtevnija faza tehnološkog procesa prerade.
            U toku ove faze u zrnu dolazi do razvoja velikog broja (i preko 1000) aromatičnih komponenti, što je najvažniji preduslov za dobijanje ukusnog napitka. Temperatura i dužina prženja u ovoj fazi igraju ključnu ulogu.</p>
          <p>Tamniji stepen prženja ima tendeciju da rezultira najčešće manjom kiselošću, izraženijom gorčinom i jačim „telom“ kafe.
            Poznavanje karakteristika svake pojedinačne kafe prema vrsti i poreklu u pogledu profila aroma koje se kod svake od kafa ravijaju tokom prženja je od neprocenjive vrednosti pri formulisanju receptura (populrno „blendova“) kafa.</p>
          <br />
          <h5>Mlevenje</h5>
          <br />
          <p>Mlevenjem se obezbeđuje povećanje slobodne površine zrna, a samim tim i lakše ekstrakcija pri pripremi napitka. Stepen usitnjenosti kafe je određen načinom pripreme napitka.</p>
          <p>Kod domaće kafe se koristi ključala voda za ekstrakciju i samo vreme ekstrakcije je najduže. Za ovu vrstu pripreme potrebno je da kafa bude što je moguće sitnije samlevena. Ovakav profil mlevenja obezbeđuje i bogatu penu na domaćoj kafi koja je jedan od najpoželjnijih atributa ovog napitka, posebno na našim prostorima.
            Prema stepenu mlevenja, nakon domaće najsitnije mlevene, dolazi espresso kafa kod koje se pri pripremi u „kašici“ aparata stvara „pogača“ od mlevene kafe, kroz koju prolazi vrela voda tokom ekstrakcije pod pritiskom koja traje optimalno 20 sekundi.
            Filter kafa je najkrupnije mlevena, jer priprema podrazumeva ekstrakciju koja se vrši u kontaktu sa vrelom vodom, a potom ceđenjem kroz filer papir.</p>
          <br />
          <h5>Priprema i serviranje kafe</h5>
          <p>Postoje četiri vrste kafe koje se svakodnevko konzumiraju širom sveta: crna kafa, espresso, filter i instant kafa, ali i mnoštvo varijacija nastalih kombinovanjem sa raznim dodacima kao što su npr. mleko, šlag, voda, alkoholna pića, čokolada, smokve, jaja, vanila itd.</p>
          <p>Vise o varijacijama možete pogledati na strani "Varijacije"</p>


        </div>
    );
  }
}
 
export default Vrste;