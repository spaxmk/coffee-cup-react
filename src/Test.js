import React, { Component } from "react";
import Pitanja from "./Test/Pitanja";
import Pitanje from "./Test/Pitanje";
import Rezultat from "./Test/Rezultat";

class Test extends Component{

	state = {
		bazaPitanja: [],
		rezultat: 0,
		pokusaji: 0
	}

	uzmiPitanja = () =>{
		Pitanja().then(pitanje => {
			this.setState({
			bazaPitanja: pitanje
			});
		});
	};

	proveriOdgovor = (odgovor, tacanOdg) => {
		if (odgovor === tacanOdg) {
			this.setState({
				rezultat: this.state.rezultat + 1
			});
		}
		this.setState({
			pokusaji: this.state.pokusaji < 4 ? this.state.pokusaji + 1 : 4
		});
	}

	componentDidMount(){
		this.uzmiPitanja();
	}

	TestPonovo = () => {
		this.uzmiPitanja();
		this.setState({
		rezultat:0,
		pokusaji:0
		});
	}

	render() {
	  	console.log(this.state)

	    return (    	
	      	<div className="row" id="content">
	      		<div className="col l12 s12">          
	          		<h2 className="center">Test provera znanja</h2>
	          		<br />
	          		{this.state.bazaPitanja.length > 0 &&
	          			this.state.pokusaji < 4 &&
						this.state.bazaPitanja.map(
						({id, pitanje, odgovori, tacanOdg}) => (
							<Pitanje key={id} 
									 pitanje={pitanje}
									 odgovori={odgovori}
									 tacanOdg={tacanOdg}
									 odgovoreno={odgovor => this.proveriOdgovor(odgovor, tacanOdg)} />
							)
					)}
	          		{this.state.pokusaji === 4 ? (<Rezultat rezultat={this.state.rezultat} TestPonovo={this.TestPonovo}/>): null}		          		
	          	</div>	          	
	      </div>
	    );
  	}

 }

export default Test;